package application;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

public class SampleController {
	
	@FXML
	private ListView<File> lvFiles;
	
	@FXML
	private Label lbPath;
	
	@FXML
	private Label lbStack;
	
	private Stack<File> stack;
	private File root;
	
	public SampleController() {
		this.stack = new Stack<File>();
		this.root = new File("/work/java/");
	}
	
	public void initialize() {
		UpFile upFile = new UpFile();
		lbPath.setText(root.getAbsolutePath());
		
		
		
		List<File> rootFiles  = new ArrayList<File>();
		rootFiles.add(upFile);
		for(File f : root.listFiles()) {
			rootFiles.add(f);
		}
		
		ObservableList<File> files = FXCollections.observableArrayList(rootFiles);
		this.lvFiles.setItems(files);
		
		
		
		this.lvFiles.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				//System.out.println("Rákattintva 1x");
				if(event.getClickCount() == 2) {
					File selected = lvFiles.getSelectionModel().getSelectedItem();
					
					//System.out.println(selected.isDirectory());
					if(selected.isDirectory() && !selected.toString().equals("..")) {
						//selected.getParentFile();
						stack.push(root);
						lbStack.setText(stactToString());
						root=selected;
						lbPath.setText(selected.getAbsolutePath());
						
						
						List<File> rootFiles  = new ArrayList<File>();
						//UpFile upFile;  //???
						rootFiles.add(upFile);
						for(File f : selected.listFiles()) {
							rootFiles.add(f);
						}
						
						lvFiles.setItems(FXCollections.observableArrayList(rootFiles));
					} else if(selected.isDirectory() && selected.toString().equals("..")) {
						root = stack.pop();
						lbStack.setText(stactToString());
						
						lbPath.setText(root.getAbsolutePath());
						List<File> rootFiles  = new ArrayList<File>();
						//UpFile upFile = new UpFile(stack.peek().getAbsolutePath());
						rootFiles.add(upFile);
						for(File f : root.listFiles()) {
							rootFiles.add(f);
						}
						lvFiles.setItems(FXCollections.observableArrayList(rootFiles));
					}
					else {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Ez egy file");
						alert.setHeaderText("Nem jó");
						alert.setContentText("A file-al nem tudunk mit kezdeni, még!");
						alert.show();
					}
				}
			}
		});
	}
	
	private String stactToString() {
		StringBuilder sb = new StringBuilder();
		for(int i=this.stack.size()-1; i>-1;i--) {
			sb.append(stack.get(i));
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}

}
